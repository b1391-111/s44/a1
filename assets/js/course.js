const adminBtn = document.getElementById("adminBtn");
const courseContainer = document.getElementById("courses");

const isAdmin = localStorage.getItem("isAdmin");
const firstName = localStorage.getItem("firstName");
const lastName = localStorage.getItem("lastName");

const url = "https://sheltered-reaches-76330.herokuapp.com/api/courses";
let courseData = [];
let cardAction;


if(isAdmin) {
    adminBtn.innerHTML = `
        <div class="mx-auto my-3">
            <a href="./create-course.html" class="btn btn-info">
                Create Course
            </a>
        </div>
    `

    fetch(url)
    .then(res => res.json())
    .then(res => {
        if(res.result <= 0){
            courseData = "No Courses Available"
        } else {
            courseData = res.result.map(course => {
                if(course.isActive) {
                    cardAction = 
                    `
                        <a href="./archiveCourse.html?courseId=${course._id}" class="btn btn-danger btn-block">
                            Archive Course
                        </a>
                        <a href="./editCourse.html?courseId=${course._id}" class="btn btn-primary btn-block">
                            Edit Course
                        </a>
                    `
                } else {
                    cardAction = 
                    `
                        <a href="./unarchiveCourse.html?courseId=${course._id}" class="btn btn-success btn-block">
                            Unarchive Course
                        </a>
                        <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-secondary btn-block">
                            Delete Course
                        </a>
                    `
                }
                return (
                    `
                        <div class="card my-3 mx-auto" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">${course.courseName}</h5>
                                <p class="card-text">${course.courseDesc}</p>
                                <p class="card-text">₱${course.price}</p>
                                <div class="card-footer">
                                    ${cardAction}
                                </div>
                            </div>
                        </div>
                    `
                )
            })
            courseContainer.innerHTML = courseData.join(" ");
        }
    });
} else {
    adminBtn.innerHTML = null;

    fetch(`${url}/active-courses`)
    .then(res => res.json())
    .then(res => {
        if(res.result <= 0){
            courseData = "No Courses Available"
        } else {
            courseData = res.result.map(course => {
                cardAction = 
                    `
                        <a href="./archiveCourse.html?courseId=${course._id}" class="btn btn-danger btn-block">
                            Archive Course
                        </a>
                        <a href="./unarchiveCourse.html?courseId=${course._id}" class="btn btn-success btn-block">
                            Unarchive Course
                        </a>
                        <a href="./archiveCourse.html?courseId=${course._id}" class="btn btn-secondary btn-block">
                            Delete Course
                        </a>
                    `
                return (
                    `
                        <div class="card my-3 mx-auto" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">${course.courseName}</h5>
                                <p class="card-text">${course.courseDesc}</p>
                                <p class="card-text">₱${course.price}</p>
                                <div class="card-footer">
                                    ${cardAction}
                                </div>
                            </div>
                        </div>
                    `
                )
            })
            courseContainer.innerHTML = courseData.join(" ");
        }
    });
}