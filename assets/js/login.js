const email = document.querySelector("#userEmail");
const pass = document.querySelector("#pass");
const loginForm = document.getElementById("loginForm"); 

const url = "https://sheltered-reaches-76330.herokuapp.com/api/users";



loginForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const userInfo = JSON.stringify({
        email: email.value, 
        password: pass.value
    });

    fetch(`${url}/login`, {
        method: "POST",
        headers: {
            "Content-Type" : "application/json"
        },
        body: userInfo
    })
        .then(res => res.json())
        .then(response => {
            if(response) {
                const {_id, firstName, lastName, email, isAdmin} = response.result;
                alert(response.message);
                localStorage.setItem("userId", _id);
                localStorage.setItem("firstName", firstName);
                localStorage.setItem("lastName", lastName);
                localStorage.setItem("email", email);
                localStorage.setItem("isAdmin", isAdmin);
                localStorage.setItem("token", response.token);
                window.location.replace("course.html");
            } else {
                alert(response.message);
            }
        })
        .catch(err => alert(err.message));
})