const params = new URLSearchParams(window.location.search);
const courseId = params.get("courseId")
const url = `https://sheltered-reaches-76330.herokuapp.com/api/courses/${courseId}/update-course-active`;

fetch(url, 
        {
            method: "PUT"
        }
    )
        .then(res => res.json())
        .then(res => {
            console.log(res);
            window.location.replace("course.html");
        });