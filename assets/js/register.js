const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const mobileNo = document.querySelector("#mobileNo");
const email = document.querySelector("#userEmail");
const pass = document.querySelector("#pass");
const confirmPass = document.querySelector("#confirmPass");

// const formBtn = document.querySelector();
const registerForm = document.getElementById("registerForm"); 

const url = "https://sheltered-reaches-76330.herokuapp.com/api/users";



registerForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const newUser = {
        firstName: firstName.value,
        lastName: lastName.value,
        mobileNo: mobileNo.value,
        email: email.value,
        password: pass.value
    }
    if(
        newUser.password === confirmPass.value &&
        newUser.mobileNo.length === 11
    ) {
        const readyNewUser = JSON.stringify(newUser);
        fetch(`${url}/register`, {
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: readyNewUser
        })
            .then(res => res.json())
            .then(res => {
                alert(res.message);
                window.location.replace("login.html");
            })
            .catch(err => alert(err.message));
    } else {
        alert("Invalid Credential")
    }
})