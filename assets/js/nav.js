const navbar = document.querySelector("nav");
const loginBtn = document.querySelector("#loginBtn");
const logoutBtn = document.querySelector("#logoutBtn");

const userId = localStorage.getItem("userId");
// const firstName = localStorage.getItem("firstName");

let isLoggedIn = `
    <a class="nav-link" href="./course.html id="loginBtn">
        Login
    </a>
`;

if(userId) {
    isLoggedIn = `
        <a class="nav-link" href="./course.html" id="logoutBtn">
            Logout
        </a>
    `
} else {
    isLoggedIn = `
        <a class="nav-link" href="./login.html" id="loginBtn">
            Login
        </a>
    `
}

navbar.innerHTML = `
    
`

loginBtn.addEventListener("click", () => {
    window.location.replace("login.html");
});

logoutBtn.addEventListener("click", (e) => {
    localStorage.clear();
    window.location.replace("index.html");
});


